class CreateViews < ActiveRecord::Migration[5.0]
  def change
    create_table :views do |t|
      t.string :name
      t.integer :crear
      t.integer :editar
      t.integer :eliminar
      t.integer :leer

      t.timestamps
    end
  end
end

class ConGenController < ApplicationController
  @@byGen = "GeneralSearch"
  def index
    if current_user
      @cu = current_user.profile_id

      if @cu != 0
        @per = Permission.where("view_name = 'GeneralSearch' and profile_id = ?", @cu)

        @per.each do |permisos|
          @uno = permisos.view_name
          @crearGen = permisos.crear
          @editarGen = permisos.editar
          @leerGen = permisos.leer
          @eliminarGen = permisos.eliminar

          if permisos.view_name == @@byGen

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        @per_c_tran = Permission.where("view_name = 'Consult Alert Transaccional' and profile_id = ?", @cu)

        @per_c_tran.each do |permisos|
          @crearConTran = permisos.crear
          @leerConTran = permisos.leer
        end

        @per_c_per = Permission.where("view_name = 'Consult Alert Perfiles' and profile_id = ?", @cu)

        @per_c_per.each do |permisos|
          @crearConPer = permisos.crear
          @leerConPer = permisos.leer
        end

        if (@@leer == 2)
          # variables que llenan los campos de búsqueda
          @layout = Tlay.all
          formato = Tformatos.all
          apps = Taplicacioneskm.all
          @tipo_reg = Ttiporegalarma.all

          base_utilizada = ActiveRecord::Base.connection.adapter_name.downcase
          base_utilizada = base_utilizada.to_sym

          case base_utilizada
            when :mysql
              sql = "SELECT user_name FROM (SELECT DISTINCT IdUsuario FROM tbitacora) AS ids JOIN users AS us ON ids.IdUsuario = us.id UNION SELECT idUsuario FROM (SELECT DISTINCT IdUsuario FROM tbitacora) AS ids WHERE idusuario NOT IN (SELECT id FROM users);"
            when :mysql2
              sql = "SELECT user_name FROM (SELECT DISTINCT IdUsuario FROM tbitacora) AS ids JOIN users AS us ON ids.IdUsuario = us.id UNION SELECT idUsuario FROM (SELECT DISTINCT IdUsuario FROM tbitacora) AS ids WHERE idusuario NOT IN (SELECT id FROM users);"
            when :sqlite
              sql = "SELECT user_name FROM (SELECT DISTINCT IdUsuario FROM tbitacora) AS ids JOIN users AS us ON ids.IdUsuario = us.id UNION SELECT idUsuario FROM (SELECT DISTINCT IdUsuario FROM tbitacora) AS ids WHERE idusuario NOT IN (SELECT id FROM users);"
            when :sqlserver
              sql = "select DISTINCT user_name from tbitacora as tb FUll JOIN users as u on CONVERT(INT, CASE WHEN IsNumeric(CONVERT(VARCHAR(12), IdUsuario)) = 1 THEN CONVERT(VARCHAR(12),IdUsuario) ELSE 0 END)  = u.id;"
              @sqlserver = true
          end

          @usuarios = ActiveRecord::Base::connection.exec_query(sql)

          gon.formato = formato
          gon.apps = apps
          gon.false = true

          # Variable para traer toda la info de Tbitacora
          @alertas = Tbitacora.all.page(params[:page]).per(20)

          # ----------------------------------------------------- BUSQUEDAS
          if params[:busqueda].present?
            query = ""
            @query = ""

            @producto = params[:product]
            @app = params[:app]
            @user = params[:user]
            @layout_b = params[:layout]
            @format = params[:format]
            @tipo_reg_b = params[:tipo_reg]
            @start_date = params[:start_date]
            @end_date = params[:end_date]
            @message = params[:message]

            if @producto != "" && !@producto.nil?
              if @producto == "1"
                query = query + "IdProducto = 1"
                @query = @query + "Producto = 'Key Monitor Transaccional'"
              elsif @producto == "2"
                query = query + "IdProducto = 2"
                @query = @query + "Producto = 'Key Monitor Perfiles'"
              end

              if @app != "" && !@app.nil?
                @app_find = Taplicacioneskm.where("id = ?", @app)

                @app_find.each do |app|
                  query = query + " and IdAplicacion = #{app.IdAplicacion}"
                  @query = @query + ", #{t('views.con_gen.app')} = #{app.Nombre}"
                end
              end
            end


            if @user != "" && !@user.nil?
              @user_find = User.find_by(user_name: @user)

              if query == ""
                if @user_find.present? && !@user_find.nil?
                  query = query + "IdUsuario = '#{@user_find.id}'"
                  @query = @query + "#{t('views.alertc_index.placeh_user')} = #{@user_find.user_name}"
                else
                  query = query + "IdUsuario = '#{@user}'"
                  @query = @query + "#{t('views.alertc_index.placeh_user')} = #{@user}"
                end
              else
                if @user_find.present? && !@user_find.nil?
                  query = query + " and IdUsuario = '#{@user_find.id}'"
                  @query = @query + ", #{t('views.alertc_index.placeh_user')} = #{@user_find.user_name}"
                else
                  query = query + " and IdUsuario = '#{@user}'"
                  @query = @query + ", #{t('views.alertc_index.placeh_user')} = #{@user}"
                end
              end
            end

            if @layout_b != "" && !@layout.nil?
              @layout_find = Tlay.find_by_IdLay(@layout_b)

              if query == ""
                query = query + "IdLay = #{@layout_b}"
                @query = @query + "Layout = #{@layout_find.Descripcion}"
              else
                query = query + " and IdLay = #{@layout_b}"
                @query = @query + ", Layout = #{@layout_find.Descripcion}"
              end

              if @format != "" && !@format.nil?
                @format_find = Tformatos.where("IdFormato = ?", @format)

                query = query + " and IdFormato = #{@format}"
                @format_find.each do |f|
                  @query = @query + ", #{t('views.con_gen.format')} = #{f.Descripcion}"
                end
              end
            end


            if @tipo_reg_b != "" && !@tipo_reg.nil?
              @tipo_reg_find = Ttiporegalarma.find_by_IdTipoReg(@tipo_reg_b)

              if query == ""
                query = query + "IdTipoReg = #{@tipo_reg_b}"
                @query = @query + "#{t('views.con_gen.type')} = #{@tipo_reg_find.Descripcion}"
              else
                query = query + " and IdTipoReg = #{@tipo_reg_b}"
                @query = @query + ", #{t('views.con_gen.type')} = #{@tipo_reg_find.Descripcion}"
              end
            end

            if (@start_date != "" && @end_date != "") && (!@start_date.nil? && !@end_date.nil?)
              @start_date = @start_date.to_date.strftime('%Y-%m-%d')
              @end_date = @end_date.to_date.strftime('%Y-%m-%d')

              if query == ""
                query = query + "Fecha between '" + @start_date.to_s + "' and '" + @end_date.to_s + "' and Hora between '00:00:00.00000' and '23:59:59.99999'"
                @query = @query + t('views.alertc_index.start_date') + " " + t('views.alertc_index.between') + " " + @start_date.to_s + " " + t('views.alertc_index.and') + " " + @end_date.to_s
              else
                query = query + " and Fecha between '" + @start_date.to_s + "' and '" + @end_date.to_s + "' and Hora between '00:00:00.00000' and '23:59:59.99999'"
                @query = @query + ", " + t('views.alertc_index.start_date') + " " + t('views.alertc_index.between') + " " + @start_date.to_s + " " + t('views.alertc_index.and') + " " + @end_date.to_s
              end
            end

            if @message != "" && !@message.nil?
              if query == ""
                query = query + "Mensaje like '%#{@message}%'"
                @query = @query + "#{t('views.alertc_index.message')} like '#{@message}'"
              else
                query = query + " and Mensaje like '%#{@message}%'"
                @query = @query + ", #{t('views.alertc_index.message')} like '#{@message}'"
              end
            end

            @alertas = Tbitacora.where(query).page(params[:page]).per(20)

            if !@alertas.present? || @alertas.count() == 0
              @error = true
              @alertas = @alertas.page(params[:page]).per(20)
            end
          end

          # ------------------------------------------------------XLS
          @xls = params[:xls_g]
          if params[:xls_g].present?
            Thread.new do
              begin
                puts red('Comienza hilo--CONSULTA GENERAL')
                @idioma = params[:idioma]
                query = ""

                @producto = params[:product]
                @app = params[:app]
                @user = params[:user]
                @layout_b = params[:layout]
                @format = params[:format]
                @tipo_reg_b = params[:tipo_reg]
                @start_date = params[:start_date]
                @end_date = params[:end_date]
                @message = params[:message]

                if (@producto != "" || @producto.nil? ) && !@producto.nil?
                  if @producto == "1"
                    query = query + "IdProducto = 1"
                  elsif @producto == "2"
                    query = query + "IdProducto = 2"
                  end

                  if (@app != "" || @app.nil?) && !@app.nil?
                    @app_find = Taplicacioneskm.find(@app)

                    query = query + " and IdAplicacion = #{@app_find.IdAplicacion}"
                  end
                end


                if (@user != "" || @user.nil?) && !@user.nil?
                  @user_find = User.find_by(:user_name => @user)

                  if query == ""
                    if @user_find.present? && !@user_find.nil?
                      query = query + "IdUsuario = '#{@user_find.id}'"
                    else
                      query = query + "IdUsuario = '#{@user}'"
                    end
                  else
                    if @user_find.present? && !@user_find.nil?
                      query = query + " and IdUsuario = '#{@user_find.id}'"
                    else
                      query = query + "and IdUsuario = '#{@user}'"
                    end
                  end
                end

                if (@layout_b != "" || @layout_b.nil?) && !@layout.nil?
                  @layout_find = Tlay.find_by_IdLay(@layout_b)

                  if query == ""
                    query = query + "IdLay = #{@layout_b}"
                  else
                    query = query + " and IdLay = #{@layout_b}"
                  end

                  if (@format != "" || @format.nil?) && !@format.nil?
                    @format_find = Tformatos.find_by_IdFormato(@format)

                    query = query + " and IdFormato = #{@format}"
                  end
                end


                if (@tipo_reg_b != "" || @tipo_reg_b.nil?) && !@tipo_reg.nil?
                  #if @tipo_reg_b.present? && !@tipo_reg.nil?
                  @tipo_reg_find = Ttiporegalarma.find_by_IdTipoReg(@tipo_reg_b)

                  if query == ""
                    query = query + "IdTipoReg = #{@tipo_reg_b}"
                  else
                    query = query + " and IdTipoReg = #{@tipo_reg_b}"
                  end
                end

                if (@start_date != "" && @end_date != "") && (!@start_date.nil? && !@end_date.nil?)
                  @start_date = @start_date.to_date.strftime('%Y-%m-%d')
                  @end_date = @end_date.to_date.strftime('%Y-%m-%d')

                  if query == ""
                    query = query + "Fecha between '" + @start_date.to_s + "' and '" + @end_date.to_s + "' and Hora between '00:00:00.00000' and '23:59:59.99999'"
                  else
                    query = query + " and Fecha between '" + @start_date.to_s + "' and '" + @end_date.to_s + "' and Hora between '00:00:00.00000' and '23:59:59.99999'"
                  end
                end

                if (@message != "" || @message.nil?) && !@message.nil?
                  if query == ""
                    query = query + "Mensaje like '%#{@message}%'"
                  else
                    query = query + " and Mensaje like '%#{@message}%'"
                  end
                end

                @alertas_xls = Tbitacora.where(query)
                @user_id = params[:user_id]
                @idioma = params[:idioma]
                puts cyan('Comienza generación de XLS-- CONSULTA GENERAL')
                create_xls = CreateXls.new
                create_xls.setInfoGenXls(@alertas_xls, @user_id, @idioma)
                create_xls.generalXls
              rescue => e
                puts red(e.to_s)
              end
            end
          end

        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  def red(mytext)
    ; "\e[31m#{mytext}\e[0m";
  end

  def green(mytext)
    ; "\e[32m#{mytext}\e[0m";
  end

  def cyan(mytext)
    ; "\e[36m#{mytext}\e[0m"
  end
end


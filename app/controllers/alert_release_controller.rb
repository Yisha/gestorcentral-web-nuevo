class AlertReleaseController < ApplicationController
  @@byAlertRelease = "AlertRelease"

  def index
    if current_user
      @id_usuario = current_user.id
      @id_aplicacion = 25
      @taplicacioneskm = "prueba"
      @idTipoReg = 11 #Index
      @fecha = Time.now.strftime("%F")
      @hora = Time.now.strftime("%k:%M:%S.00000")
      @men = "AlertRelease Index"


      @tbit = Tbitacora.new
      @tbit.IdUsuario = @id_usuario
      @tbit.IdAplicacion = @id_aplicacion
      @tbit.IdTipoReg = @idTipoReg #Index
      @tbit.Fecha = @fecha
      @tbit.Hora  = @hora
      @tbit.Mensaje = @men
      @tbit.save
      @cu = current_user.profile_id

      if @cu != 0

        @per = Permission.where("view_name = 'AlertRelease' and profile_id = ?", @cu)

        @per.each do |permisos|
          @uno = permisos.view_name
          @crearLiberacion = permisos.crear
          @editarLiberacion = permisos.editar
          @leerLiberacion = permisos.leer
          @eliminarLiberacion = permisos.eliminar

          if permisos.view_name == @@byAlertRelease

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        if current_user.habilitado == 0

          if ((@@editar == 4) || (@@leer == 2))
            @atendiendo = Tatendiendo.all.page(params[:page]).per(20)

            if params[:release].present?
              @alerta_libera = params[:id_alert]
              @alerta_libera = Tatendiendo.find(@alerta_libera)

              if @alerta_libera.present?
                @alerta_libera.destroy
              end

              @atendiendo = Tatendiendo.all.page(params[:page]).per(20)
            end
          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_enabled')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end
end

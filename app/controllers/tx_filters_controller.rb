class TxFiltersController < ApplicationController
  before_action :set_tx_filters, only: [:show, :edit, :update, :destroy]

  # GET /tx_filters
  # GET /tx_filters.json
  def index
    @tx_filters = TxFilters.all
  end

  # GET /tx_filters/1
  # GET /tx_filters/1.json
  def show
  end

  # GET /tx_filters/new
  def new
    @tx_filter = TxFilters.new

    tFormatos = Tformatos.all
    gon.formatos = tFormatos

    tCampos = Tcamposformato.all
    gon.tCamposFormatos = tCampos

    tLays = Tcamposlay.all
    gon.tCamposLays = tLays
    gon.false = true

  end

  # GET /tx_filters/1/edit
  def edit

    # Gon para traes los campos del formato y layout correspondiente
    @tx_sessions = TxSessions.all
    tx_sessions = TxSessions.all
    gon.sesiones = tx_sessions

    ## Gon para llenar selects Layout, Formato de ventana modal

    tCampos = Tcamposformato.all
    gon.tCamposFormatos = tCampos

    tLays = Tcamposlay.all
    gon.tCamposLays = tLays
    gon.false = true

  end

  # POST /tx_filters
  # POST /tx_filters.json
  def create


    @tx_filter = TxFilter.create(tx_filters_params)
    # respond_to do |format|
    #   if @tx_filter.save
    #     format.html { redirect_to @tx_filter, notice: 'Tx filter was successfully created.' }
    #     format.json { render :show, status: :created, location: @tx_filter }
    #   else
    #     format.html { render :new }
    #     format.json { render json: @tx_filter.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # PATCH/PUT /tx_filters/1
  # PATCH/PUT /tx_filters/1.json
  def update

    @tx_filter.update(tx_filters_params)


    # respond_to do |format|
    #   if @tx_filter.update(tx_filters_params)
    #     format.html { redirect_to action: :edit, notice: 'Tx filter was successfully updated.' }
    #     format.json { render :show, status: :ok, location: @tx_filter }
    #   else
    #     format.html { render :edit }
    #     format.json { render json: @tx_filter.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # DELETE /tx_filters/1
  # DELETE /tx_filters/1.json
  def destroy


    @tx_filter.destroy
    respond_to do |format|
      format.html { redirect_to tx_sessions_index_path, notice: 'El filtro fue eliminado exitosamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tx_filters
      @tx_filter = TxFilters.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tx_filters_params
      params.require(:tx_filters).permit(:Session_id, :Group_id, :Description, :Backgroundcolor, :Lettercolor, :Order_number, :Key_id)
    end
end

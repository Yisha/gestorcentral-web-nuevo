//= require datapicker/bootstrap-datepicker.js
//= require orden
//= require shearch_colum

console.log('EntreAAlertTran');
/*------------------------------ Codigo Shearch Jose Luis */

//Si esta hay algo en observaciones o selecionado algun estado - no se redireciona
$('.linkTran').on('click', function(){
    // alert('Moviiii link')

    let obseArea = $('#txObse').val(),
        estado = $('#state_ob').val(),
        id = $(this).html();
    // alert(id)

    //Traducciones
    let titleAlert = $('#titleAlert').val(),
        datosAlert = $('#datosAlert').val(),
        loseData = $('#loseData').val(),
        btn_continue = $('#btn_continue').val(),
        btn_yes = $('#btn_yes').val(),
        btn_cancel = $('#btn_cancel').val();



    if( obseArea != "" || estado != "" ){
        swal({
                title: `${titleAlert}`,
                text: `${datosAlert}`,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                // confirmButtonText: `${btn_yes}`,
                confirmButtonText: `${btn_continue}`,
                // cancelButtonText: `${btn_cancel}`,
                cancelButtonText: `${btn_cancel}`,
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function (isConfirm) {

                if (isConfirm){

                    swal({
                            title: `${titleAlert}`,
                            // text: `${yourSure}`,
                            text: `${loseData}`,
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            // confirmButtonText: `${btn_confir}`,
                            confirmButtonText: `${btn_continue}`,
                            // cancelButtonText: `${btn_cancel2}`,
                            cancelButtonText: `${btn_cancel}`,
                            closeOnConfirm: false,
                            closeOnCancel: false
                        },
                        function (isConfirm) {

                            if (isConfirm){

                                // Se envia el ajax para eliminar el filtro
                                // window.location.href = `/tx_filters/${id}/edit`
                                window.location.href = `/alertn/alertTran?id=${id}`

                            }
                            else{
                                swal(`Cancelado`,`Tus datos estan seguros`, "error");
                            }
                        });

                } else {
                    swal(`Cancelado`,`Tus datos estan seguros`, "error");
                }
            });
    } else {
        window.location.href = `/alertn/alertTran?id=${id}`
    }

});

$(document).ready(function () {
    //--- No muestra contenido de la información
    $('.rdiv').css('display', 'none')

    //---- Agrega clases y atibutos a los inputs de la búsqueda de los inputs
    $('td').children('input').addClass('form-control input-sm').attr('placeholder', 'Search');

    //---- Total de Inputs que hay en el Dom
    var totalHijos = $('td').children('input').length;

    //---- Se divide el total en el 100% del ancho del contenedor
    var width = 100 / totalHijos;

    // console.log('Soy total de ancho ${width}')

    // console.log('Soy total hijos ${totalHijos}')

    //---- Agrega CSS  'ancho' a las 'td' de la tabla
    $('.datosTabla').css('width', '${width}%')


    $('caption').addClass('stickyRows');
    $('#thead').addClass('stickyTableSearch');
    //----- Position Absolute para Barra de inputs
    // $('#tableSearch').scroll(function () {
    //     console.log("Soy scroll Tabla busqueda" + $(this).scrollTop());
    //
    //     if ($(this).scrollTop() > 73) {
    //         $('#thead').addClass('fixedTableSearch');
    //         $('caption').addClass('strickyTableSearch);
    //         $('#table1').css('margin-top', '12.5em');
    //     } else {
    //         $('#thead').removeClass('fixedTableSearch');
    //         $('caption').removeClass('strickyTableSearch');
    //         $('#table1').css('margin-top', '0');
    //     }
    // });
    //----- Position Absolute para Barra de inputs
});
//
var filtersConfig = {
    base_path: 'scripts/tablefilter',
    // col_1: 'select',
    // col_2: 'select',
    // col_3: 'select',
    // alternate_rows: true,
    rows_counter: true,
    // btn_reset: true,
    // loader: true,
    alternate_rows: true,
    mark_active_columns: true,
    highlight_keywords: true,
    no_results_message: true,
};

// var tf = new TableFilter(document.querySelector('.table1'));
var tf = new TableFilter('table_tran', filtersConfig);
tf.init();




/* Orden De Campos */ /*---------------------------- Orden de Campos Alfred*/

$(function () {
    var orden = {}

    // Sortable rows
    $('.sorted_table').sortable({
        containerSelector: 'table',
        itemPath: '> tbody',
        itemSelector: 'tr',
        placeholder: '<tr class="placeholder"/>'
    });

    // Sortable column heads
    var oldIndex;
    var oldName;
    $('.sorted_head tr').sortable({
        containerSelector: 'tr',
        itemSelector: 'th',
        placeholder: '<th class="placeholder"/>',
        vertical: false,
        onDragStart: function ($item, container, _super) {
            oldIndex = $item.index();
            oldName = $item.text();
            $item.appendTo($item.parent());
            _super($item, container);
        },
        onDrop: function ($item, container, _super) {
            var newIndex = $item.index();
            if (newIndex != oldIndex) {
                $item.closest('table').find('tbody tr').each(function (i, row) {
                    row = $(row);
                    if (newIndex < oldIndex) {
                        row.children().eq(newIndex).before(row.children()[oldIndex]);
                    } else if (newIndex > oldIndex) {
                        row.children().eq(newIndex).after(row.children()[oldIndex]);
                    }
                });
            }
            _super($item, container);
            orden[newIndex] = $('.sorted_head th').attr('id');
        }
    });

    $('.sorted_head2 tr').sortable({
        containerSelector: 'tr',
        itemSelector: 'th',
        placeholder: '<th class="placeholder"/>',
        vertical: false,
        onDragStart: function ($item, container, _super) {
            oldIndex = $item.index();
            oldName = $item.text();
            $item.appendTo($item.parent());
            _super($item, container);
        },
        onDrop: function ($item, container, _super) {
            var newIndex = $item.index();

            if (newIndex != oldIndex) {
                $item.closest('table').find('tbody tr').each(function (i, row) {
                    row = $(row);
                    if (newIndex < oldIndex) {
                        row.children().eq(newIndex).before(row.children()[oldIndex]);
                    } else if (newIndex > oldIndex) {
                        row.children().eq(newIndex).after(row.children()[oldIndex]);
                    }
                });
            }
            _super($item, container);
            orden[newIndex] = $('.sorted_head th').attr('id');
        }
    });
});

/*** Guardo los datos en la tabla "TableOrders" ***/

$('.save_first').click(function () {
    var orden = {}
    var ordenD = {}
    var contador = 0;
    var contadorD = 0;
    var tabla = $('.sorted_head').attr('id');
    var tabla2 = $('.sorted_head2').attr('id');

    /* ID de inputs para mostrar los mensajes ES/EN */
    var tra_tit = $('#tit_save_tran').val()
    var mes_tit = $('#mes_save_tran').val()
    var save_area_work = $("#save_tran").val()

    $('.sorted_head tr th').each(function () {
        contador += 1
        var valor = $(this).text()
        orden[contador] = valor
    })

    $('.sorted_head2 tr th').each(function () {
        contadorD += 1
        var valor = $(this).text()
        //console.log("Posicion: "+contador+" Valor: "+valor)
        ordenD[contadorD] = valor
    })

    $.ajax({
        type: "POST",
        beforeSend: function (xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        },
        url: "/table_orders",
        contentType: "application/json",
        data: JSON.stringify({
            "_method": "create",
            table: tabla,
            table2: tabla2,
            orden: orden,
            ordenD: ordenD
        }),
        success: function (data, txt, file) {
            if(data.message == true){

                swal (tra_tit,mes_tit,"success")
                //$(".save_first").text(save_area_work)
                $("#mnj_save_work").text(save_area_work)
            }
            /*console.log("Data: " + data.message)
            console.log("TXT: " + txt)
            console.log("FILE: " + file)*/
        }
    })
})

/* Demas Codigo que se encontraba en la vista */
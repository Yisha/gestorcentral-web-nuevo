require 'test_helper'

class GestorSettingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @gestor_setting = gestor_settings(:one)
  end

  test "should get index" do
    get gestor_settings_url
    assert_response :success
  end

  test "should get new" do
    get new_gestor_setting_url
    assert_response :success
  end

  test "should create gestor_setting" do
    assert_difference('GestorSetting.count') do
      post gestor_settings_url, params: { gestor_setting: {blue_day: @gestor_setting.blue_day, blue_nigth: @gestor_setting.blue_nigth, red_day: @gestor_setting.red_day, red_night: @gestor_setting.red_nigth, yellow_day: @gestor_setting.yellow_day, yellow_night: @gestor_setting.yellow_nigth } }
    end

    assert_redirected_to gestor_setting_url(GestorSetting.last)
  end

  test "should show gestor_setting" do
    get gestor_setting_url(@gestor_setting)
    assert_response :success
  end

  test "should get edit" do
    get edit_gestor_setting_url(@gestor_setting)
    assert_response :success
  end

  test "should update gestor_setting" do
    patch gestor_setting_url(@gestor_setting), params: { gestor_setting: {blue_day: @gestor_setting.blue_day, blue_nigth: @gestor_setting.blue_nigth, red_day: @gestor_setting.red_day, red_night: @gestor_setting.red_nigth, yellow_day: @gestor_setting.yellow_day, yellow_night: @gestor_setting.yellow_nigth } }
    assert_redirected_to gestor_setting_url(@gestor_setting)
  end

  test "should destroy gestor_setting" do
    assert_difference('GestorSetting.count', -1) do
      delete gestor_setting_url(@gestor_setting)
    end

    assert_redirected_to gestor_settings_url
  end
end

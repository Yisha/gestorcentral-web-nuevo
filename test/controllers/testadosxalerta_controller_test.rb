require 'test_helper'

class TestadosxalertaControllerTest < ActionDispatch::IntegrationTest
  setup do
    @testadosxalerta = testadosxalerta(:one)
  end

  test "should get index" do
    get testadosxalerta_index_url
    assert_response :success
  end

  test "should get new" do
    get new_testadosxalerta_url
    assert_response :success
  end

  test "should create testadosxalerta" do
    assert_difference('Testadosxalerta.count') do
      post testadosxalerta_index_url, params: { testadosxalerta: {  } }
    end

    assert_redirected_to testadosxalerta_url(Testadosxalerta.last)
  end

  test "should show testadosxalerta" do
    get testadosxalerta_url(@testadosxalerta)
    assert_response :success
  end

  test "should get edit" do
    get edit_testadosxalerta_url(@testadosxalerta)
    assert_response :success
  end

  test "should update testadosxalerta" do
    patch testadosxalerta_url(@testadosxalerta), params: { testadosxalerta: {  } }
    assert_redirected_to testadosxalerta_url(@testadosxalerta)
  end

  test "should destroy testadosxalerta" do
    assert_difference('Testadosxalerta.count', -1) do
      delete testadosxalerta_url(@testadosxalerta)
    end

    assert_redirected_to testadosxalerta_index_url
  end
end
